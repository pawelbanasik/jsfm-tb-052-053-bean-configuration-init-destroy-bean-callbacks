package com.pawelbanasik;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.pawelbanasik.domain.Organization;

public class InitDestroyApp {
	public static void main(String[] args) {

		ApplicationContext context = new FileSystemXmlApplicationContext("beans.xml");
		
//		Organization organization = (Organization) context.getBean("myorg");
//		
//		System.out.println(organization.corporateSlogan());
//
//		System.out.println(organization);
//		
//		System.out.println(organization.corporateService());
		
		((AbstractApplicationContext) context).close();
		
		
	}
}
