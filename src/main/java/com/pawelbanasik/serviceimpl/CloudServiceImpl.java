package com.pawelbanasik.serviceimpl;

import java.util.Random;

import com.pawelbanasik.service.BusinessService;

public class CloudServiceImpl implements BusinessService {

	public String offeringService(String companyName) {
		Random random = new Random();
		String service = "\nAs an Organization, " + companyName + " offers world class Cloud computing infrastructure."
				+ "\nThe annual income exceeds " + random.nextInt(revenue) + " dollars.";

		return service;

	}

	public void postConstruct() {
		System.out.println("CloudService: postConstruct called.");
	}

	public void preDestroy() {
		System.out.println("CloudService: preDestroy called.");
	}
 }
